# Week 1 - a
### Basic git operation
1. init a new git repository, it should has a main branch
2. add a file main.js
```
function main() {
    // empty
}

main();
```
3. commit main.js

## Week 1 - b
### Branch
1. checkout a new branch from main branch
2. add file lib_math.js
```
function add(a, b) {
    return a + b;
}

function sub(a, b) {
    return a - b;
}
```

3. commit lib_math.js

4. merge back to main branch

5. what kind of strategy can we use when merging it?

   ```
   git checkout master
   git merge <branch to merge>
   ```

## Week 1 - c

### Think of
1. in b, we add the whole lib_math.js in a commit, how do you think of it?

A: commit had better be `function oriented`, which means, when we update/modify the function, we should add a new commit.

2. If there's better way, how will you fix it? Please write down the steps.

A: All `git rebase (more complicated)`, `git commit -amend` and `git reset` can fix it.