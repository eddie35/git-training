# Git Training

## Common commands

+ `git stash`
+ `git rebase`
+ `git git reset --mixed/--soft/--hard`
+ `git commit --amend`
+ `git merge --no-ff/--ff/--ff-only`
+ `git merge -s `
